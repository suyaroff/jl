<?php
return array(
	'catalog1' => array(
		'parent_id' => 0,
		'title' => 'Покемоны',
		'active' => 1,
		'deleted' => 0,
	),
	'catalog2' => array(
		'parent_id' => 1,
		'title' => 'Первое поколение',
		'active' => 1,
		'deleted' => 0,
	),
	'catalog3' => array(
		'parent_id' => 1,
		'title' => 'Второе поколение',
		'active' => 1,
		'deleted' => 0,
	),
);