<?php
class CreatedBehaviorTest extends CDbTestCase
{
	public $fixtures = array(
		'pages' => 'Page'
	);
	
	public function testBeforeSave()
	{
		$page = new Page();
		
		$page->title = 'New page';
		$page->deleted = 0;
		$page->url = 'new';
		
		$time = date('Y-m-d H:i:s');
		$this->assertEquals(TRUE, $page->save());
		
		$this->assertEquals($time, $page->created);
	}
	
	
}