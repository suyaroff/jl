<?php
class DeletedBehaviorTest extends CDbTestCase
{
	public $fixtures = array(
		'pages' => 'Page'
	);
	
	public function testRemove()
	{
		$page = Page::model()->findByPk(1);
		$page->remove();
		$this->assertEquals(TRUE, $page->save());
		
		$p = Page::model()->findByPk(1);
		$this->assertEquals(1, $p->deleted);
	}
	
	public function testRestore()
	{
		$page = Page::model()->findByPk(2);
		$page->restore();
		$this->assertEquals(TRUE, $page->save());
		
		$p = Page::model()->findByPk(2);
		$this->assertEquals(0, $p->deleted);
	}
	
	public function testNotRemoved()
	{
		$pages = new Page();
		$p = $pages->notRemoved()->findAll();
		
		// Подсчитываем
		$notRemovedCount = 0;
		foreach ($this->pages as $i) {
			if ($i['deleted'] == 0) $notRemovedCount++;
		}
		
		$this->assertEquals($notRemovedCount, count($p));
	}
	
	public function testRemoved()
	{
		$pages = new Page();
		$p = $pages->removed()->findAll();
		
		// Подсчитываем
		$removedCount = 0;
		foreach ($this->pages as $i) {
			if ($i['deleted'] == 1) $removedCount++;
		}
		
		$this->assertEquals($removedCount, count($p));
	}
	
	public function testRemoved2()
	{
		// Подсчитываем
		$removedCount = 0;
		$allCount = 0;
		foreach ($this->pages as $i) {
			if ($i['deleted'] == 1) $removedCount++;
			$allCount++;
		}
		
		$pages = Page::model()->removed()->findAll();
		
		$this->assertEquals($removedCount, count($pages));
		
		$pages = Page::model()->findAll();
		
		$this->assertEquals($allCount, count($pages));
	}
	
	public function testIsRemove()
	{
		$page = Page::model()->removed()->find();
		$this->assertEquals(TRUE, $page->isRemoved());
	}
}