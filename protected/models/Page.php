<?php

/**
 * This is the model class for table "pages".
 *
 * The followings are the available columns in table 'pages':
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property integer $deleted
 */
class Page extends CActiveRecord
{
	protected  $fullUrl = NULL;
	
	public function behaviors()
	{
		return array(
			'deleted' => array(
				'class' => 'application.modules.drycms.components.behaviors.DeletedBehavior',
			),
			'created' => array(
				'class' => 'application.modules.drycms.components.behaviors.CreatedBehavior',
			),
			'active' => array(
				'class' => 'application.modules.drycms.components.behaviors.ActiveBehavior',
			),
			'tree' => array(
				'class' => 'application.modules.drycms.components.behaviors.TreeBehavior',
			),
			'position' => array(
				'class' => 'application.modules.drycms.components.behaviors.PositionBehavior',
			),
		);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('active', 'required'),
			array('deleted', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, url, deleted', 'safe', 'on'=>'search'),
			array('id, title, body, url, controller', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'children' => array(CActiveRecord::HAS_MANY,'Page', 'parent_id'),
			'parent' => array(CActiveRecord::BELONGS_TO,'Page', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Заголовок',
			'body' => 'Текст',
			'url' => 'URL',
			'deleted' => 'Удален',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('deleted',$this->deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Возвращает полный url страницы
	 */
	public function getFullUrl()
	{
		if ($this->fullUrl === NULL) {
			$this->fullUrl = $this->url;
			
			$parent = $this->parent;
			while ($parent !== NULL) {
				$this->fullUrl = $parent->url.'/'.$this->fullUrl;
				$parent = $parent->parent;
			}
			
			$this->fullUrl = '/'.$this->fullUrl;
		}
		
		return $this->fullUrl;
	}

	public function fields()
	{
		return array(
			'title' => array('type' => 'textbox', 'table'),
			'body' => array('type' => 'html'),
			'url' => array('type' => 'textbox', 'table'),
			'controller' => array('type' => 'textbox', 'table'),
		);
	}
}