<div class="clearfix">
	<?php echo $this->form->labelEx($this->record, $this->attribute); ?>
	<div class="input">
		<?php $this->widget('DField'.ucfirst($this->field['type']), array(
			'form' => $this->form,
			'record' => $this->record,
			'attribute' => $this->attribute,
			'field' => $this->field,
		)); ?>
	</div>
</div>
