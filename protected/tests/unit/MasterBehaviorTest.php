<?php
class MasterBehaviorTest extends CDbTestCase
{
	public $fixtures = array(
		'items' => 'Item',
	);
	
	
	
	public function testMasterColumn()
	{
		$item = new Item();
		
		$structure = array(
			'page' => array(
				'label' => 'Страницы',
				'model' => 'Page',
			),
			'article' => array(
				'label' => 'Статьи',
				'model' => 'Article',
			),
			'catalog' => array(
				'label' => 'Каталог',
				'model' => 'Catalog',
			),
			'item' => array(
				'master' => array('sectionId' => 'catalog', 'relation' => 'items'), // ключ массива хозяина
				'label' => 'Позиции',
				'model' => 'Item',
				
			),
		);
		
		$item->attachBehavior('master', array('class' => 'application.modules.drycms.components.behaviors.MasterBehavior', 'structure' => $structure, 'section' => 'item'));
		
		
		$this->assertEquals('catalog_id', $item->masterColumn);
		
	}
	
	public function testMasterColumnNoExist()
	{
		$page = new Page();
		
		$structure = array(
			'page' => array(
				'label' => 'Страницы',
				'model' => 'Page',
			),
			'article' => array(
				'label' => 'Статьи',
				'model' => 'Article',
			),
			'catalog' => array(
				'label' => 'Каталог',
				'model' => 'Catalog',
			),
			'item' => array(
				'master' => array('sectionId' => 'catalog', 'relation' => 'items'), // ключ массива хозяина
				'label' => 'Позиции',
				'model' => 'Item',
				
			),
		);
		
		$page->attachBehavior('master', array('class' => 'application.modules.drycms.components.behaviors.MasterBehavior', 'structure' => $structure, 'section' => 'page'));
		
		
		$this->assertEquals(FALSE, $item->masterColumn);
	}
	
	public function testMaster()
	{
		$item = new Item();
		
		$structure = array(
			'page' => array(
				'label' => 'Страницы',
				'model' => 'Page',
			),
			'article' => array(
				'label' => 'Статьи',
				'model' => 'Article',
			),
			'catalog' => array(
				'label' => 'Каталог',
				'model' => 'Catalog',
			),
			'item' => array(
				'master' => array('sectionId' => 'catalog', 'relation' => 'items'), // ключ массива хозяина
				'label' => 'Позиции',
				'model' => 'Item',
				
			),
		);
		
		$item->attachBehavior('master', array('class' => 'application.modules.drycms.components.behaviors.MasterBehavior', 'structure' => $structure, 'section' => 'item'));
		
		$records = $item->master(2)->findAll();
		
		// Подсчитываем элементы
		$count = 0;
		foreach ($this->items as $i) {
			if ($i['catalog_id'] == 2) $count++;
		} 
		
		$this->assertEquals(2, count($records));
	}
	
	public function testMasterNoExist()
	{
		$page = new Page();
		
		$structure = array(
			'page' => array(
				'label' => 'Страницы',
				'model' => 'Page',
			),
			'article' => array(
				'label' => 'Статьи',
				'model' => 'Article',
			),
			'catalog' => array(
				'label' => 'Каталог',
				'model' => 'Catalog',
			),
			'item' => array(
				'master' => array('sectionId' => 'catalog', 'relation' => 'items'), // ключ массива хозяина
				'label' => 'Позиции',
				'model' => 'Item',
				
			),
		);
		
		$page->attachBehavior('master', array('class' => 'application.modules.drycms.components.behaviors.MasterBehavior', 'structure' => $structure, 'section' => 'page'));
		
		$records = $page->master(2)->findAll();
		
	}
}